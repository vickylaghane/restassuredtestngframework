package common_utilities;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;

public class Handle_API_logs {

	// Objective : Creating directory inside our project

	@BeforeTest
	public static File Create_Log_Directory(String DirectoryName) {
		String Current_project_directory = System.getProperty("user.dir"); // It will give current project directory.
		System.out.println(Current_project_directory);

		File Log_Directory = new File(Current_project_directory + "//API_Logs//" + DirectoryName);
		Delete_Directory(Log_Directory);
		Log_Directory.mkdir(); // Creates Directory.
		System.out.println(Log_Directory);
		return Log_Directory;
	}

	@BeforeTest
	public static boolean Delete_Directory(File Directory) {

		boolean Directory_deleted = Directory.delete();

		if (Directory.exists()) {

			File[] files = Directory.listFiles();

			// Get all the names of the files(in array) present in the given directory.

			if (files != null) {
				for (File file : files) {
					if (file.isDirectory()) {
						Delete_Directory(file); // Called Recursion/Recursive method -->Calling same method inside the
												// method.
					} else {
						file.delete();
					}
				}
			} else {
				Directory_deleted = Directory.delete();
			}
		} else {
			System.out.println("Directory does not exist.");
		}
		return Directory_deleted;
	}

	@AfterTest
	public static void evidence_creator(File dir_name, String file_name, String endpoint, String requestbody,
			String responsebody) throws IOException {

//Step-1 --> Create the file at given location
		File new_file = new File(dir_name + "\\" + file_name + ".txt");
		System.out.println("New file created to save evidence: " + new_file.getName());

//Step-2 --> Write the data into the file
		FileWriter data_writer = new FileWriter(new_file);
		data_writer.write("Endpoint is: " + endpoint + "\n");
		data_writer.write("Request Body is: \n" + requestbody + "\n");
		data_writer.write("Response Body is: \n" + responsebody);
		data_writer.close();
		System.out.println("Evidence is written in file: " + new_file.getName());
	}
}
