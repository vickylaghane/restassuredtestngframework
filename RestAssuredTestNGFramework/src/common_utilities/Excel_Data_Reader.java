package common_utilities;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.testng.annotations.BeforeTest;

public class Excel_Data_Reader {
	
	@BeforeTest
	public static ArrayList<String> readExcelData(String filename, String sheetname, String testcasename)
			throws IOException {

		ArrayList<String> arraydata = new ArrayList<String>();

//-->	Step 1: Locate the Excel file
		String project_dir = System.getProperty("user.dir");

		FileInputStream fis = new FileInputStream(project_dir + "\\Input_Data\\" + filename);

//-->	Step 2: Access the located Excel file
		XSSFWorkbook wb = new XSSFWorkbook(fis);

//-->	Step 3: Count the number of sheets available in located Excel file
		int countofsheet = wb.getNumberOfSheets();
		System.out.println("number of sheets: " + countofsheet);

//-->	Step 4: Access the desired sheet
		for (int i = 0; i < countofsheet; i++) {
			String Sheet_name = wb.getSheetName(i);
			if (Sheet_name.equals(sheetname)) {
				System.out.println("Inside the sheet: " + Sheet_name);
				XSSFSheet sheet = wb.getSheetAt(i);
				Iterator<Row> rows = sheet.iterator();
				while (rows.hasNext()) {
					Row currentrow = rows.next();

//-->	Step 5: Access the row corresponding to desired Test Case
					if (currentrow.getCell(0).getStringCellValue().equals(testcasename)) {
						Iterator<Cell> cell = currentrow.iterator();
						while (cell.hasNext()) {
							String data = cell.next().getStringCellValue();
							//System.out.println(data);
							arraydata.add(data);
						}
					}
				}

			}
		}
		wb.close();
		return arraydata;
	}
}
