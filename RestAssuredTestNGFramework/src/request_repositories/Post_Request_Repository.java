package request_repositories;

import java.io.IOException;
import java.util.ArrayList;

import org.testng.annotations.BeforeTest;

import common_utilities.Excel_Data_Reader;

public class Post_Request_Repository extends Endpoints {
	
	@BeforeTest
	public static String post_TC1_Request() throws IOException {
		
		ArrayList<String> exceldata = Excel_Data_Reader.readExcelData("Test_WorkBook.xlsx", "Post_API", "Post_TC1");
		System.out.println(exceldata);
		// based on the indexes we can change the values that need to be passed in Request Body
		String req_name = exceldata.get(1);
		String req_job = exceldata.get(2);
		
		
		String requestBody = "{\r\n"
				+ "    \"name\": \""+req_name+"\",\r\n"
				+ "    \"job\": \""+req_job+"\"\r\n"
				+ "}";
		
		return requestBody;
	}
}
