package request_repositories;

public class Endpoints {

	static String hostname = "https://reqres.in/";
	
	public static String post_endpoint() {
		String URL = hostname+"api/users";
		
		return URL;
	}
	
	public static String get_endpoint() {
		String URL = hostname+"api/users?page=2";
		
		return URL;
	}
	
	public static String put_endpoint() {
		String URL = hostname+"api/users/2";
		
		return URL;
	}
	
	public static String patch_endpoint() {
		String URL = hostname+"api/users/2";
		
		return URL;
	}
	
}
