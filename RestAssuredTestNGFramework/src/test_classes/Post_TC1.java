package test_classes;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;

import org.testng.Assert;
import org.testng.annotations.Test;

import common_methods.Trigger_Post_API_Method;
import common_utilities.Handle_API_logs;
import io.restassured.path.json.JsonPath;

public class Post_TC1 extends Trigger_Post_API_Method {
	
	@Test
	public static void executor() throws IOException {
		
		String requestBody = post_TC1_Request();
		
		File dir_name = Handle_API_logs.Create_Log_Directory("Post_TC1");

		for (int i = 0; i < 5; i++) {
			int Status_Code = extract_Status_Code(requestBody, post_endpoint());
			System.out.println("Status Code: " + Status_Code);

			if (Status_Code == 201) {
				String ResponseBody = extract_Response_Body(requestBody, post_endpoint());
				System.out.println("Response Body :" + ResponseBody);
				
				Handle_API_logs.evidence_creator(dir_name, "Post_TC1", post_endpoint(), requestBody, ResponseBody);
				
				validator(requestBody,ResponseBody);
				break;
			} else {
				System.out.println("Desired Status Code not found hence, retry");
			}
		}

	}

	public static void validator(String requestBody,String ResponseBody) throws IOException {
		
		LocalDateTime CurrentDate = LocalDateTime.now();
		String ExpectedDate = CurrentDate.toString().substring(0,11);
		
		JsonPath jsp_req = new JsonPath(requestBody);
		
		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");

		JsonPath jsp_res = new JsonPath(ResponseBody);
		
		String res_name = jsp_res.getString("name");
		String res_job = jsp_res.getString("job");
		String res_createdAt = jsp_res.getString("createdAt").substring(0,11);

		// Validation
		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
		Assert.assertEquals(ExpectedDate, res_createdAt);

	}

}
