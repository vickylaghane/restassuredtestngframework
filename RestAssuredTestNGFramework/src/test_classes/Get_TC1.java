package test_classes;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.testng.Assert;
import org.testng.annotations.Test;

import common_methods.Trigger_Get_API_Method;
import common_utilities.Handle_API_logs;
import io.restassured.path.json.JsonPath;

public class Get_TC1 extends Trigger_Get_API_Method {
	
	@Test

	public static void executor() throws IOException {
		
		File dir_name = Handle_API_logs.Create_Log_Directory("Get_TC1");

		for (int i = 0; i < 5; i++) {
			int Status_Code = extract_StatusCode(get_endpoint());
			System.out.println(Status_Code);

			if (Status_Code == 200) {
				String ResponseBody = extract_ResponseBody(get_endpoint());
				System.out.println(ResponseBody);
				
				Handle_API_logs.evidence_creator(dir_name, "Get_TC1", get_endpoint(), null , ResponseBody);

				validator(ResponseBody);
				break;
			} else {
				System.out.println("Desired Status Code is invalid hence retry");
			}
		}
	}

	public static void validator(String ResponseBody) {
		
		String[] Exp_id_Array = { "7", "8", "9", "10", "11", "12" };
		String[] Exp_email_Array = {"michael.lawson@reqres.in","lindsay.ferguson@reqres.in","tobias.funke@reqres.in",
									"byron.fields@reqres.in","george.edwards@reqres.in","rachel.howell@reqres.in"};
		String[] Exp_first_name_Array = {"Michael","Lindsay","Tobias","Byron","George","Rachel"};
		String[] Exp_last_name_Array = {"Lawson","Ferguson","Funke","Fields","Edwards","Howell"};
		String[] Exp_avatar_Array = {"https://reqres.in/img/faces/7-image.jpg","https://reqres.in/img/faces/8-image.jpg","https://reqres.in/img/faces/9-image.jpg",
									 "https://reqres.in/img/faces/10-image.jpg","https://reqres.in/img/faces/11-image.jpg","https://reqres.in/img/faces/12-image.jpg"};


		JsonPath jsp_res = new JsonPath(ResponseBody);

		int res_page = jsp_res.getInt("page");
		Assert.assertEquals(res_page, 2);
		
		int res_per_page = jsp_res.getInt("per_page");
		Assert.assertEquals(res_per_page, 6);
		
		int res_total = jsp_res.getInt("total");
		Assert.assertEquals(res_total, 12);
		
		int res_total_pages = jsp_res.getInt("total_pages");
		Assert.assertEquals(res_total_pages, 2);
		
		List<Object> res_data = jsp_res.getList("data");
		int count = res_data.size();
		//System.out.println("Total Count of Objects: "+count);
		
		for (int i = 0; i < count; i++) {
			
			String Exp_id = Exp_id_Array[i];
			String res_id = jsp_res.getString("data[" + i + "].id");
			Assert.assertEquals(res_id, Exp_id);
			
			String Exp_email = Exp_email_Array[i];
			String res_email = jsp_res.getString("data["+i+"].email");
			Assert.assertEquals(res_email, Exp_email);
			
			String Exp_first_name = Exp_first_name_Array[i];
			String res_first_name = jsp_res.getString("data["+i+"].first_name");
			Assert.assertEquals(res_first_name, Exp_first_name);
			
			String Exp_last_name = Exp_last_name_Array[i];
			String res_last_name = jsp_res.getString("data["+i+"].last_name");
			Assert.assertEquals(res_last_name, Exp_last_name);
			
			String Exp_avatar = Exp_avatar_Array[i];
			String res_avatar = jsp_res.getString("data["+i+"].avatar");
			Assert.assertEquals(res_avatar, Exp_avatar);
		}

	}
}
