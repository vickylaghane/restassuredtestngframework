package test_classes;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;

import org.testng.Assert;
import org.testng.annotations.Test;

import common_methods.Trigger_Patch_API_Method;
import common_utilities.Handle_API_logs;
import io.restassured.path.json.JsonPath;

public class Patch_TC1 extends Trigger_Patch_API_Method {
	
	@Test

	public static void executor() throws IOException {
		
		String requestbody = patch_TC1_Request();

		File dir_name = Handle_API_logs.Create_Log_Directory("Patch_TC1");

		for (int i = 0; i < 5; i++) {
			int Status_Code = extract_Status_Code(requestbody, patch_endpoint());
			System.out.println("Status Code: " + Status_Code);

			if (Status_Code == 200) {
				String ResponseBody = extract_Response_Body(requestbody, patch_endpoint());
				System.out.println("Response Body :" + ResponseBody);

				Handle_API_logs.evidence_creator(dir_name, "Patch_TC1", patch_endpoint(), requestbody,ResponseBody);

				validator(requestbody,ResponseBody);
				break;

			} else {
				System.out.println("Desired Status Code not found hence, retry");
			}
		}

	}

	public static void validator(String requestbody,String ResponseBody) {

		LocalDateTime CurrentDate = LocalDateTime.now();
		String ExpectedDate = CurrentDate.toString().substring(0, 11);

		JsonPath jsp_req = new JsonPath(requestbody);

		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");

		JsonPath jsp_res = new JsonPath(ResponseBody);

		String res_name = jsp_res.getString("name");
		String res_job = jsp_res.getString("job");
		String res_updatedAt = jsp_res.getString("updatedAt").substring(0, 11);

		// Validation
		Assert.assertEquals(req_name, res_name);
		Assert.assertEquals(req_job, res_job);
		Assert.assertEquals(ExpectedDate, res_updatedAt);

	}
}
