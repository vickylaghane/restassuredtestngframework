package common_methods;

import static io.restassured.RestAssured.given;

import request_repositories.Endpoints;

public class Trigger_Get_API_Method extends Endpoints{

		public static int extract_StatusCode(String url) {
			int StatusCode = given()
							 .when().get(url)
							 .then().extract().statusCode();
			
			return StatusCode;
		}
		
		public static String extract_ResponseBody(String url) {
			String ResponseBody = given()
								  .when().get(url)
								  .then().extract().response().asString();
			
			return ResponseBody;
		}
}
