package common_methods;

import static io.restassured.RestAssured.given;

import request_repositories.Put_Request_Repository;

public class Trigger_Put_API_Method extends Put_Request_Repository{

	public static int extract_Status_Code(String req_Body, String url) {
		int StatusCode = given().header("Content-Type","application/json").body(req_Body)
				 .when().put(url)
				 .then().extract().statusCode();
		return StatusCode;
	}
	
	public static String extract_Response_Body(String req_Body, String url ) {
		String responseBody = given().header("Content-Type","application/json").body(req_Body)
							 .when().put(url)
							 .then().extract().response().asString();
		return responseBody;
	}
}
