This project consists of a framework which is designed specifying testing of APIs by triggering of various requests such as GET,PATCH,POST,PUT,DELETE. This framework created by using Java and OOPs concepts (Inheritance,Encapsulation), TestNG, Maven (build tool), POM, and Restassured, Eclipse(IDE tool). This framework is systematic & structured, means the representation of various packages containing various classes which consists of dynamic & reusable methods which helps in testing the APIs in easy & reliable way.

It provides a optimized code in the form of methods which is convenient and expressive to perform various operations required during testing the APIs.

Structure of the framework in terms of packages :

** request_repository (package) -> This package contains different classes defining methods that returns the endpoints details as well as request body parameters for different API requests.

** common_methods (package) -> This package contains specific classes that defines common methods that returns the response status codes and response body results, which can be used by every request that we trigger while testing the APIs. These defined methods acts as core part of the RestAssured framework, as these methods performing the major tasks of the framework.

** common_utility (package) -> This package contains specific classes that defines common utilities in terms of methods that can be used commonly during or after the execution of the requests, like storing the responses, test data, logs into the files, so as to debugg & monitor the results as well as reading the input data from Excel files as a part of Data Driven testing, making the execution in a sequence & in systematic and organized order.

** test_classes (package) -> This package contains different classes for separate requests defining methods that performs the execution & validation of actual & expected results for each & every request that is triggered.

** TestNG -> Use of TestNG framework helps in simplifying the testing. It provides various TestNG annotations like, @BeforeTest, @Test, @AfterTest,etc as well as "testng.xml" file by using which execution of tests can be done from a single point by creating "Suite".
 - It helps in execution of the tests in sequenctial order by using annotations such as "Priority", as well as takes less time for execution, which improves the overall performance of the test execution from designed framework.
 - It also offers report generation such as html reports, allure-reports (using allure report dependency), extent-reports (using extent report dependency) which helps in analyzing the results.
